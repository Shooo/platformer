#ifndef PLATFORMER_INPUT_H
#define PLATFORMER_INPUT_H

#include <stdbool.h>
#include <SDL2/SDL.h>


struct Input {
    // a lot of useless keys, but it's too much of a pain to change right now
    // could also reduce the size by compacting 8 keys in a single Uint8 since they're boolean anyways
    Uint8 *keyboardState;
    double *keyboardTimes;

    // flag for quitting when pressing escape or the cross
    bool shouldQuit;

    // time management, might be moved out later if output needs it
    double now;
    double delta;
};

typedef struct Input Input;

Input* create_input();
void fetch_input(Input *input);
void create_keyboard(Input *input);
void update_keyboard(Input *input);
void destroy_input(Input *input);

// only works with PHYSICAL layout for now
// we could use SDL_GetScancodeFromKey, I just don't want to lengthen the function names
// maybe we should swap to solely virtual instead?
bool is_down(Input *input, SDL_Scancode key);
bool is_up(Input *input, SDL_Scancode key);
bool just_pressed(Input *input, SDL_Scancode key);
bool just_released(Input *input, SDL_Scancode key);
double key_delay(Input *input, SDL_Scancode key);

#endif //PLATFORMER_INPUT_H