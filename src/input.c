#include "input.h"

Input* create_input() {
    Input *input = (Input*) malloc(sizeof(Input));
    create_keyboard(input);
    input->shouldQuit = false;
    input->now = (float) (SDL_GetTicks()) / 1000;
    input->delta = 0;
    return input;
}

void create_keyboard(Input *input) {
    int kbsize;
    const Uint8 * keyboard = SDL_GetKeyboardState(&kbsize);
    input->keyboardState = (Uint8*) malloc(kbsize * sizeof(Uint8));
    input->keyboardTimes = (double*) malloc(kbsize * sizeof(double));
    for (int i = 0; i < kbsize; i++) {
        input->keyboardState[i] = keyboard[i];
        input->keyboardTimes[i] = input->now;
    }
}

void update_keyboard(Input *input) {
    int kbsize;
    const Uint8 * keyboard = SDL_GetKeyboardState(&kbsize);
    for (int i = 0; i < kbsize; i++) {
        if(input->keyboardState[i] != keyboard[i]) {
            input->keyboardTimes[i] = input->now;
        }
        input->keyboardState[i] = keyboard[i];
    }
}

void fetch_input(Input *input) {
    // update time
    double last = input->now;
    input->now = (float) (SDL_GetTicks()) / 1000;
    input->delta = input->now - last;

    // filter events
    SDL_Event e;
    while(SDL_PollEvent(&e)) {
        switch(e.type) {
            case SDL_KEYDOWN:
                switch (e.key.keysym.sym) {
                    case SDLK_ESCAPE:
                        input->shouldQuit = true;
                        break;
                }
                break;
            case SDL_QUIT:
                input->shouldQuit = true;
                break;
        }
    }
    update_keyboard(input);
}

bool is_down(Input *input, SDL_Scancode key) {
    return input->keyboardState[key];
}

bool is_up(Input *input, SDL_Scancode key) {
    return !input->keyboardState[key];
}

bool just_pressed(Input *input, SDL_Scancode key) {
    return input->keyboardTimes[key] == input->now && input->keyboardState[key];
}

bool just_released(Input *input, SDL_Scancode key) {
    return input->keyboardTimes[key] == input->now && !input->keyboardState[key];
}

double key_delay(Input *input, SDL_Scancode key) {
    return input->now - input->keyboardTimes[key];
}

void destroy_input(Input *input) {
    free(input->keyboardState);
    free(input->keyboardTimes);
    free(input);
}