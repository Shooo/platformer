#include "game.h"
#include <stdio.h>

int main() {
    Game *game = create_game();
    run_game(game);
}