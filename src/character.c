#include "character.h"
#include <game.h>

void create_character(Character *character, double x, double y) {
    Character temp = {
            .x = x, .y = y,
            .vx = 0, .vy = 0,
            .w = 50, .h = 50
    };
    *character = temp;
}

void tick_character(Character *character, int floorY, Input* input) {
    double delta = input->delta;
    double th = 0.20;
    double h = 100.0;
    double ay = 2.0 * h / (th * th);

    // update x velocity based on movement
    character->vx = 0;
    if(is_down(input, SDL_SCANCODE_D)) {
        character->vx += 500.0;
    } if(is_down(input, SDL_SCANCODE_A)) {
        character->vx -= 500.0;
    }

    // update y velocity on jump
    if(just_pressed(input, SDL_SCANCODE_SPACE)) {
        double v0 = -2.0 * h / th;
        character->vy = v0;
    }
    // add gravity to y velocity
    character->vy += ay * delta;

    // update position
    character->x += character->vx * delta;
    character->y += character->vy * delta + 0.5 * ay * delta * delta;
    // stops on floor
    if(character->y > floorY - character->h) {
        character->y = floorY - character->h;
        character->vy = 0;
    }
}

void render_character(Character *character, Output *output) {
    SDL_Rect rect = {
        character->x,
        character->y,
        character->w,
        character->h
    };
    SDL_SetRenderDrawColor(output->renderer, 127, 127, 127, 255);
    SDL_RenderFillRect(output->renderer, &rect);
}

void destroy_character(Character *character) {

}