#include "game.h"

Game* create_game() {
    SDL_Init(SDL_INIT_EVERYTHING);

    Game *game = (Game*) malloc(sizeof(Game));

    game->output = create_output();
    game->input = create_input();

    game->floorY = game->output->height * 3 / 4;
    create_character(&game->character, (float) game->output->width / 2 - 25, game->floorY - 50);

    return game;
}

void tick_game(Game *game) {
    tick_character(&game->character, game->floorY, game->input);
}

void render_game(Game *game) {
    Output *output = game->output;

    SDL_SetRenderDrawColor(output->renderer, 255, 255, 255, 255);
    SDL_RenderClear(output->renderer);

    SDL_SetRenderDrawColor(output->renderer, 0, 0, 0, 255);
    SDL_RenderDrawLine(output->renderer, 0, game->floorY, output->width, game->floorY);

    SDL_SetRenderDrawColor(output->renderer, 200, 200, 200, 255);
    SDL_RenderDrawLine(output->renderer, 0, game->floorY - 100, output->width, game->floorY - 100);

    render_character(&game->character, output);

    SDL_RenderPresent(output->renderer);
}

_Noreturn void run_game(Game *game) {
    while(true) {
        fetch_input(game->input);
        if(game->input->shouldQuit)
            quit_game(game);
        tick_game(game);
        render_game(game);
    }
}

void quit_game(Game *game) {
    destroy_character(&game->character);

    destroy_output(game->output);
    destroy_input(game->input);
    free(game);

    SDL_Quit();
    exit(0);
}